#!/bin/bash

BS_V_Ma=0
BS_V_Mi=1
BS_V_Pa=0
BS_V_Tw=0
BS_V_TXT=$BS_V_Ma.$BS_V_Mi.$BS_V_Pa.$BS_V_Tw


help_text(){

	echo "Usage:"
	echo ' '
	echo '    $0 \[option]'s
	echo " "
	echo "        -a            build all versions"
	echo "        -d            build debug"
	echo "        -h            display this help"
	echo "        -p            build profile"
	echo "        -r            build release"
	echo "        -v            display version invormation"
	echo " "
	
}

version_text(){

	echo "Build Script version $BS_V_TXT"
	echo " "
	
}
premake_setup(){
	
	premake5 --to=./build/files --bin=./bin --lib=./lib gmake
}


build_debug(){
	
	mingw32-make config=debug
	
}

build_profile(){
	mingw32-make config=profile
}

build_release(){
	mingw32-make config=release
}

build_type(){

	case $1 in
		
		-v)
			version_text
			;;
		-h)
			version_text
			help_text
			;;
		-a)
			version_text
			echo Building everything
			premake_setup
			build_debug
			build_profile
			build_release
			;;
		-d)
			version_text
			echo "Building debug version"
			premake_setup
			build_debug
			;;
		-p)
			version_text
			premake_setup
			echo "Building profile version"
			build_profile
			;;
		-r)
			version_text
			echo "Building release version"
			premake_setup
			build_release
			;;
		*)
			echo "Error: <$1> Unreconized option"
			echo " "
			help_text
			;;
		
	esac
	
}


case  $# in
	1)
		build_type $1
		
		;;
	
	*)
		help_text
		
		;;

esac

